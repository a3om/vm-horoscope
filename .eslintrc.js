module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  extends: ['prettier', 'plugin:prettier/recommended', 'plugin:nuxt/recommended'],
  plugins: ['prettier'],
  // add your custom rules here
  rules: {
    'no-undef': 'off',
    'no-unused-vars': ['warn'],
    'no-console': 'off', // ['warn'],
    'prefer-const': 'off',
    'require-await': 'off', // ['warn'],
    'no-constant-condition': 'off', // ['warn'],
    'no-unreachable': 'off', // ['warn'],
    'no-useless-catch': 'off',
    'no-lonely-if': 'off',
  },
};

process.env.VERSION = process.env.npm_package_version;
process.env.APP_ENV = process.env.APP_ENV || (process.env.NODE_ENV === 'production' ? 'production' : 'local');
const isDevelopment = process.env.APP_ENV === 'development';
const isLocal = process.env.APP_ENV === 'local';
const isProduction = process.env.APP_ENV === 'production';

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'horoscope',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
      { name: 'apple-mobile-web-app-status-bar-style', content: 'black-translucent' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~assets/styles/main.scss',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~plugins/interface',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    ['@nuxtjs/eslint-module', { fix: true }],
    // https://go.nuxtjs.dev/stylelint
    ['@nuxtjs/stylelint-module', { fix: true }],
    // https://google-fonts.nuxtjs.org
    '@nuxtjs/google-fonts',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://www.npmjs.com/package/@nuxtjs/style-resources
    '@nuxtjs/style-resources',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  styleResources: {
    scss: ['~/assets/styles/helpers.scss'],
  },

  googleFonts: {
    families: {
      Roboto: [100, 400, 700],
    },
    prefetch: true,
  },

  server: {
    host: process.env.APP_HOST || '0.0.0.0',
    port: process.env.APP_PORT || 3000,
  },

  vue: {
    config: {
      productionTip: true,
      devtools: !isProduction,
      silent: isProduction,
      performance: true,
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    devtools: !isProduction,

    filenames: {
      app: () => (isLocal ? '[name].js' : isDevelopment ? '[name].[contenthash].js' : '[contenthash].js'),
      chunk: () => (isLocal ? '[name].js' : isDevelopment ? '[name].[contenthash].js' : '[contenthash].js'),
      css: () => (isLocal ? '[name].css' : isDevelopment ? '[name].[contenthash].css' : '[contenthash].css'),

      img: () =>
        isLocal
          ? '[path][name].[ext]'
          : isDevelopment
          ? 'img/[name].[contenthash:7].[ext]'
          : 'img/[contenthash:7].[ext]',

      font: () =>
        isLocal
          ? '[path][name].[ext]'
          : isDevelopment
          ? 'fonts/[name].[contenthash:7].[ext]'
          : 'fonts/[contenthash:7].[ext]',

      video: () =>
        isLocal
          ? '[path][name].[ext]'
          : isDevelopment
          ? 'videos/[name].[contenthash:7].[ext]'
          : 'videos/[contenthash:7].[ext]',
    },

    optimization: { minimize: isProduction },
    transpile: [],

    /*
     ** You can extend webpack config here
     */
    extend(config, context) {
      config.module.rules.push({
        test: /\.(ogg|mp3|wav|mpe?g)$/i,
        loader: 'file-loader',
        options: { name: '[path][name].[ext]' },
      });
    },
  },
};
